// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "position.h"
#include "entity.h"
#include "game.h"
#include "gamerules.h"
#include "tile.h"
#include "maze.h"
#include "astar.h"
#include <cstdint>
#include <utility>
#include <iostream>
#include <stack>
#include <set>

using std::vector;
using std::set;
using std::make_pair;
using std::stack;
using std::cout;
using std::endl;

// need to find the closest hero in astarchasehero
// with access to game
AStar::AStar(Maze *m, Position &start, Position &end) {
  this->m_maze = m;
  this->m_start = start;
  this->m_goal = end;
}

AStar::~AStar() { }

int AStar::h(Position &curr, Position &goal) const {
  return curr.distanceFrom(goal);
}

int AStar::posToIndex(Position &pos) {
  return pos.getY() * m_maze->getWidth() + pos.getX();
}

bool AStar::allowMove(Game *g, Entity *e, Position &curr, Position &to) {
  return g->getGameRules()->allowMove(g, e, curr, to);
}

Direction AStar::trace(vector<Bundle> b) {
  /* using the stack to trace path so that
     the Position that will be immediately adjacent 
     to the current Position can be returned by popping
     off the first element on stack once tracing is complete.
  */
  stack<Position> path;

  // the goal here would be the nearest hero
  Position pos = m_goal;
  int index = this->posToIndex(pos);

  // using the posToIndex function, we can convert a Position
  // to a number as the index, tracing starting with the goal
  // Position, back to the origin; when the adjacent position
  // to the start is reached, end the loop.
  while (!(b[index].prev_pos == pos)) {
    path.push(pos);
    Position temp = b[index].prev_pos;
    pos = temp;
    index = this->posToIndex(pos);
  }

  // get the top of the stack and return its Direction
  // relative to the start.
  if (!path.empty()) {
    Position next = path.top();
    path.pop();
    if (m_start.displace(Direction::UP) == next) {
      return Direction::UP;
    } else if (m_start.displace(Direction::LEFT) == next) {
      return Direction::LEFT;
    } else if (m_start.displace(Direction::DOWN) == next) {
      return Direction::DOWN;
    } else if (m_start.displace(Direction::RIGHT) == next) {
      return Direction::RIGHT;
    }
  }
  
  // default: stay still
  return Direction::NONE;
}

Direction AStar::search(Game *g, Entity *e) {
  
  // astarchasehero finds the nearest hero

  int w = m_maze->getWidth();
  int h = m_maze->getHeight();

  // closed list implemented as a vector
  // of bools of size of the maze, w * h
  // to indicate whether the Position at
  // a specific index has been explored
  vector<bool> closed(w * h, 0);
  // the vector of Bundles store the f, g
  // and h values associated with the Position
  // at its index
  vector<Bundle> bund;
  // reserve space for bund in case of seg
  // faults
  bund.reserve(w * h);

  // open list implemented as a set of Pairs
  // of a Position's f value and Position,
  // and the pairs in a set are stored in
  // sorted order by the first value so that
  // the Position of the lowest f-value gets
  // explored first
  set<Pair> open;

  // initialize Bundles and the f, g and h
  // values all to positive infinity
  for (int i = 0; i < h * w; i++)  {
    bund.push_back(Bundle());
    bund[i].f = INT8_MAX;
    bund[i].g = INT8_MAX;
    bund[i].h = INT8_MAX;
  }

  // mark the starting Position as explored
  // and initialize f, g and h values to 0.
  // Mark its previous index as itself.
  int start_index = posToIndex(m_start);
  bund[start_index].f = 0;
  bund[start_index].g = 0;
  bund[start_index].h = 0;
  bund[start_index].prev_pos = m_start;

  // initialize the open list with starting
  // Position
  open.insert(make_pair(0, m_start));
  bool found = 0;

  // while there are still nodes to explore
  // in the open list, explore the nodes
  while (!open.empty()) {
    // get the Pair with the least f-value to expand
    Pair p = *open.begin();
    open.erase(open.begin());
    // p.first is f value
    Position curr_pos = p.second;
    int index = posToIndex(curr_pos);
    closed[index] = 1;
    
    //
    int new_g, new_f, new_h;
    // --------------------------------------------------------------
    // Direction::UP
    Position up = curr_pos.displace(Direction::UP);
    int new_index = posToIndex(up);
    // reached end
    if (up == m_goal) {
      // goal reached, return trace path
      bund[new_index].prev_pos = curr_pos;
      found = 1;
      return trace(bund);
    } else if (!closed[new_index] && allowMove(g, e, curr_pos, up)) {
      // if not goal, then add the up Position to the open node
      // for future expansions

      // the new_g increments the current Position's g
      // as the total number of moves made to go from the
      // starting Position to the up Position
      new_g = bund[index].g + 1;
      // the cost to move from current Position to the 
      // up Position
      new_h = this->h(curr_pos, up);
      // the new f-value for up Position
      new_f = new_g + new_h;
      if (bund[new_index].f == INT8_MAX || bund[new_index].f > new_f) {
        open.insert(make_pair(new_f, up));
	bund[new_index].f = new_f;
	bund[new_index].g = new_g;
	bund[new_index].h = new_h;
	bund[new_index].prev_pos = curr_pos;
      }
    }

    // Direction::LEFT
    Position left = curr_pos.displace(Direction::LEFT);
    new_index = posToIndex(left);
    // reached end
    if (left == m_goal) {
      bund[new_index].prev_pos = curr_pos;
      found = 1;
      return trace(bund);
    } else if (!closed[new_index] && allowMove(g, e, curr_pos, left)) {
      new_g = bund[index].g + 1;
      new_h = this->h(curr_pos, left);
      new_f = new_g + new_h;
      if (bund[new_index].f == INT8_MAX || bund[new_index].f > new_f) {
        open.insert(make_pair(new_f, left));
	bund[new_index].f = new_f;
	bund[new_index].g = new_g;
	bund[new_index].h = new_h;
	bund[new_index].prev_pos = curr_pos;
      }
    }

    // Direction::DOWN
    Position down = curr_pos.displace(Direction::DOWN);
    new_index = posToIndex(down);
    // reached end
    if (down == m_goal) {
      bund[new_index].prev_pos = curr_pos;
      found = 1;
      return trace(bund);
    } else if (!closed[new_index] && allowMove(g, e, curr_pos, down)) {
      new_g = bund[index].g + 1;
      new_h = this->h(curr_pos, down);
      new_f = new_g + new_h;
      if (bund[new_index].f == INT8_MAX || bund[new_index].f > new_f) {
        open.insert(make_pair(new_f, down));
	bund[new_index].f = new_f;
	bund[new_index].g = new_g;
	bund[new_index].h = new_h;
	bund[new_index].prev_pos = curr_pos;
      }
    }

    // Direction::RIGHT
    Position right = curr_pos.displace(Direction::RIGHT);
    new_index = posToIndex(right);
    // reached end
    if (right == m_goal) {
      bund[new_index].prev_pos = curr_pos;
      found = 1;
      return trace(bund);
    } else if (!closed[new_index] && allowMove(g, e, curr_pos, right)) {
      new_g = bund[index].g + 1;
      new_h = this->h(curr_pos, right);
      new_f = new_g + new_h;
      if (bund[new_index].f == INT8_MAX || bund[new_index].f > new_f) {
        open.insert(make_pair(new_f, right));
	bund[new_index].f = new_f;
	bund[new_index].g = new_g;
	bund[new_index].h = new_h;
	bund[new_index].prev_pos = curr_pos;
      }
    }
  }
  
  // if goal is not found, stay still
  if (!found) {
    return Direction::NONE;
  }

  // default: stay still
  return Direction::NONE;
}
