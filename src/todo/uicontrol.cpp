// Maddie Chabab
// Lisa: unfinished
#include "uicontrol.h"
#include "ui.h"
#include "game.h"
#include "entity.h"

UIControl::UIControl() { }

UIControl::~UIControl() { }

Direction UIControl::getMoveDirection(Game *g, Entity *) {
  return (g->getUI())->getMoveDirection();
}

bool UIControl::isUser() const override { return true; }

