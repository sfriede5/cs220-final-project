//Sydney Friedel
#include "game.h"
#include "maze.h"
#include "ui.h"
#include "position.h"
#include "entity.h"
#include "gamerules.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include "ecfactory.h"

Game::Game(){
  m_maze = new Maze(0, 0);
  m_UI = new UI();
  m_gameRules = new GameRules();
}

Game::~Game(){
  delete m_maze;
  delete m_ui;
  delete m_gameRules;
  for(EntityVec::iterator it = m_entities.begin();
      it != m_entities.end(); it++){
    delete *it;
  }
 
}

void Game::setMaze(Maze *maze){
  m_maze = maze;
}

void Game::setUI(UI *ui){
  m_ui = ui;
}

void Game::setGameRules(GameRules *gameRules){
  m_gameRules = gameRules;
}

void Game::addEntity(Entity *entity){
  m_entities.push_back(entity);
}

Entity * Game::getEntityAt(const Position &pos){
  //iterate through each of the entities and compare their pos
  //to the specified one
  for(EntityVec::iterator it = m_entities.begin();
      it != m_entities.end(); it++){
    Position cur_pos = it->getPosition();
    //if this entity's current position equals the specified entity
    //return this entity
    if(cur_pos == pos){ return it; }
  }
  //if we got this far, none of the entities are at that position
  return nullptr;
}

const EntityVec & Game::getEntities() const{
  const EntityVec &vec = *m_entities;
  return vec;
}

EntityVec Game::getEntitiesWithProperty(char prop) const{
  //EntityVec to hold entities with property
  EntityVec vec;
  //iterator through this game's entities and find the ones with property
  for(EntityVec::iterator it = m_entities.begin();
      it != m_entities.end(); it++){
    //if it has that property, add it to the new EntityVec
    if(it->hasProperty(prop)){ vec.push_back(it); }
  }
  return vec;
}

Maze * Game::getMaze(){ return m_maze; }

UI * Game::getUI(){ return m_ui; }

GameRules * Game::getGameRules(){ return m_gameRules; }

void Game::gameLoop(){
  //create iterator to be updated in round robin fashion
  EntityVec::iterator it = m_entities.begin();
  //get initial game result (should be UNKNOWN)
  GameResult game_result = m_gameRules->checkGameResult(*this);
  //continue taking turns until game result changes from UNKNOWN
  while(game_result == UNKNOWN){
    //if it's the hero's turn, display game state
    if(it->m_controller->isUser()){
      m_ui->render(*this);
    }
    //allow the current entity to take a turn
    takeTurn(it);
    //update gameResult
    game_result = m_gameRules->checkGameResult(*this);
    //figure out which entity goes next
    if(it + 1 == m_entities.end()){
      it = m_entities.begin();
    } else {
      it++;
    }
  }
  //display final game state
  if(game_result == HERO_WINS){
    std::string &msg = "Hero wins \n";
    m_ui->displayMessage(msg, true);
  } else {
    std::string &msg = "Hero loses \n";
    m_ui->displayMessage(msg, true);
  }
}

void Game::takeTurn(Entity *actor){
  //get proposed move from entity controller
  Direction dir = actor->m_controller->getMoveDirection(*this, actor);
  //See if gameRules allows move
  Position p = actor->getPosition();
  const Position& pos = p;
  Position d = p->displace(dir);
  const Position& dest = d;
  bool allowed = m_gameRules->allowMove(*this, actor, pos, dest);
  //if move is allowed, enact the move
  if(allowed){
    m_gameRules->enactMove(*this, actor, dest);
  } else if(actor->m_controller->isUser()){
    std::string &msg = "Illegal move \n";
    m_ui->displayMessage(msg, false);   
  }  
}

static Game * Game::loadGame(std::istream &in){
  //read in and store maze contents
  Maze *m = Maze::read(in);
  
  if(m == nullptr){ return nullptr; }
  setMaze(m);
  //read in and store entity contents
  EntityControllerFactory *efactory = EntityControllerFactory::getInstance();
  std::string next_code;
  int x = 0;
  int y = 0;
  while(in >> next_code >> x >> y){
    //check to make sure game data is valid
    const Position &pos(x, y);
    if(!inBounds(pos)){ return nullptr; }
    //create new entity and add it to m_entities
    Entity *new_ent = decodeEntity(next_code, x, y, ecfactory);
    addEntity(new_ent);
  }
  }
  return *this;
}

Entity * Game::decodeEntity(std::string code, int x, int y,
			    EntityControllerFactory *ecfactory){
  Entity *ent = new Entity;
  char gly = code.at(0);
  const std::string &glyph(1, gly);
  ent->setGlyph(glyph);
  char ec = code.at(1);
  EntityController *controller = ecfactory->createFromChar(ec);
  ent->setController(controller);
  char prop = code.at(2);
  const std::string &props(1, prop);
  ent->setProperties(props);
  const Position &pos(x, y);
  ent->setPosition(pos);
  return ent;
  

}
