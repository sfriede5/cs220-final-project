#ifndef CHASEHERO_H
#define CHASEHERO_H

#include "entitycontroller.h"
#include "game.h"
#include "entity.h"

class ChaseHero : public EntityController {
private:
  // disallow copy constructor and assignment operator
  ChaseHero(const ChaseHero &);
  ChaseHero &operator=(const ChaseHero &);

public:
  ChaseHero();
  virtual ~ChaseHero();

  virtual Direction getMoveDirection(Game *game, Entity *entity);
  virtual bool isUser() const;

private:
  // Add your own private member functions...
  // Returns true if hero can make a move, false if not 
  virtual bool canMove(Game *game, Entity *minotaur, const Position &minotaurPos, const Position &dest) const; //virtual?  
};

#endif // CHASEHERO_H
