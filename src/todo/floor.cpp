//Sydney Friedel
#include "floor.h"
#include "position.h"
#include "entity.h"
#include <string>


Floor::Floor() { }
Floor::~Floor() { }
virtual MoveResult Floor::checkMoveOnto(Entity *entity, const Position &fromPos
					,const Position &tilePos) const override {
  (void)entity;
  // check how much the entity is moving, cannot be 
  // greater than 1
  if (fromPos.distanceFrom(tilePos) == 1) {
    return MoveResult::ALLOW;
  }
  return MoveResult::BLOCK;
}

virtual bool Floor::isGoal() const override { return false; }

virtual std::string Floor::getGlyph() const override{
  std::string str = ".";
  return str;
}
