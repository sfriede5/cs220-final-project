//Maddie Chabab
#include "textui.h"

TextUI::TextUI () { }

TextUI::~TextUI() { }

Direction TextUI::getMoveDirection() {
  char userMove;
  cout << "Your move (u/d/l/r):";
  cin >> userMove;
  if (userMove == 'u') {
    return Direction userMove = Direction::UP; 
  }
  else if (userMove == 'd') {
    return Direction userMove = Direction::DOWN;
  }
  else if (userMove == 'l') {
    return Direction userMove = Direction::LEFT;
  }
  else if (userMove == 'r') {
    return Direction userMove = Direction::RIGHT;
  }
  else {
    cout << "unknown direction";
    cout << endl;
    return(getMoveDirection());
  }
}

displayMessage(const std::string &msg, bool endgame) {
  m_msg = msg; //message is not displayed until render is called 
}

render(Game *game) {  
  for (EntityVec::iterator it = game->m_entities.begin(), int count = 0;
       it != game->m_entities.end(); it++, count++) {
    cout << *it;
    if (count % game->m_maze.getWidth() == 0) {
      cout << endl;
    }
  }
  if (m_msg != NULL) {
    cout << ": " << m_msg; //display a waiting message
    cout << endl;
  }  
  //add special case - will vector already be updated with all correct entries?
}
   
  


