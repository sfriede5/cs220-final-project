// Maddie Chabab

#include "chasehero.h"
#include <vector>
#include <math.h>

ChaseHero::ChaseHero() { }

ChaseHero::~ChaseHero() { }

Direction ChaseHero::getMoveDirection(Game *game, Entity *entity) {
  EntityVec heros = game->getEntitiesWithProperty('h');
  int count = 0; 
  int prevDist, currDist;   
  int currX, currY, prevX, prevY;
  int heroX, heroY;
  
  //Minotaur position to be used in canMove
  Position mPos = entity->getPosition();
  const Position& minotaurPos = mPos; 
  
  //Determine closest hero and its x,y coordinates  
  for (EntityVec::iterator it = heros.begin();
       it != heros.end(); it++) {
    //assuming "shortest" as determined by distanceFrom of position class
    currDist = (it->getPosition()).distanceFrom(mPos);
    currX = (it->getPosition())->getX();
    currY = (it->getPosition())->getY();
    if (count != 0) {
      if (currDist < prevDist) {
	heroX  = currX;
	heroY  = currY; 
      }
      else {
	heroX  = prevX;
	heroY  = prevY;
      }
      //find x and y of closest hero
    }
    prevDist = currDist;
    prevX = currX;
    prevY = currY; 
    count=count+1;
  }
  
  //Determine recommended move for Minotaur
  int minotaurX = (mPos)->getX();
  int minotaurY = (mPos)->getY();
  Direction minoMove = Direction::NONE;
  Position u = mPos->displace(Direction::UP);
  const Position& destU = u;
  Position d = mPos->displace(Direction::DOWN);
  const Position& destD = d; 
  Position l = mPos->displace(Direction::LEFT);
  const Position& destL = l;
  Position r = mPos->displace(Direction::RIGHT);
  const Position& destR = r;
  
  if ( abs(minotaurX - heroX) > abs(minotaurY - heroY) ||
       !(this->canMove(game, entity, minotaurPos, destL)) ||
       !(this->canMove(game, entity, minotaurPos, destR)) ) { 
    //Hero is farther vertically or cannot move horizontally 
    if (heroX < minotaurX) {
      return minoMove = Direction::UP; 
    }
    else if (heroX > minotaurX) {
      return minoMove = Direction::DOWN;
    }
  }
  else if ( abs(minotaurX - heroX) > abs(minotaurY - heroY) ||
	    abs(minotaurX - heroX) = abs(minotaurY - heroY) ||
	    !(this->canMove(game, entity, minotaurPos, destU)) ||
	    !(this->canMove(game, entity, minotaurPos, destD)) ) {
    //Hero is either farther horizontally, equally far horizontal/vertical,
    //or cannot move vertically 
    if (heroY < minotaurY) {
      return minoMove = Direction::LEFT;
    }
    else if (heroY > minotaurY) {
      return minoMove = Direction::RIGHT;
    }
  }
}


bool ChaseHero::isUser() const override { return false; }

bool ChaseHero::canMove(Game *game, Entity *minotaur, const Position &minotaurPos, const Position &dest) const override {
  theseRules = game->getGameRules();
  bool allowed = theseRules->allowMove(game, minotaur, minotaurPos, dest); 
  return allowed; 
}
