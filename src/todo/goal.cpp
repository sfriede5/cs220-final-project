//Sydney Friedel
#include "goal.h"
#include "entity.h"
#include "position.h"
#include <string>

Goal::Goal() { }
Goal::~Goal() { }
virtual MoveResult Goal::checkMoveOnto(Entity *entity, const Position &fromPos, const Position &tilePos) const override{

  (void)entity;
  if(fromPos.distanceFrom(tilePos) == 1){ return MoveResult::ALLOW; }
  return MoveResult::BLOCK;
}

virtual bool Goal::isGoal() const override{ return true; }
virtual std::string Goal::getGlyph() const override{
  std::string str = "<";
  return str;
}
