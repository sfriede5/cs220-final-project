//Sydney Friedel
#include "wall.h"
#include "position.h"
#include "entity.h"
#include <string>


Wall::Wall(){
}
Wall::~Wall(){
}
virtual MoveResult Wall::checkMoveOnto(Entity *entity, const Position &fromPos,
				       const Position &tilePos) const override{
  (void)entity;
  (void)fromPos;
  (void)tilePos;
  return MoveResult::BLOCK;
  
}

virtual bool Wall::isGoal() const override{ return false; }

virtual std::string Wall::getGlyph() const override{
  std::string str = "#";
  return str;
}

