//Sydney Friedel
#include "maze.h"
#include "tile.h"
#include "entity.h"
#include "position.h"
#include "tilefactory.h"
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>

Maze::Maze(int w, int h){
  width = w;
  height = h;
}

Maze::~Maze(){
  for(std::vector<Tile*>::iterator it = tiles.begin();
      it != tiles.end(); it++){
    delete *it;
  }
  delete tiles;
}

int Maze::getWidth() const{ return width; }
int Maze::getHeight() const { return height; }

bool Maze::inBounds(const Position &pos) const{
  return ((position->x >= 0) && (position->x < width)
	  && (position->y >= 0) && (position->y < height));
				       
}

void Maze::setTile(const Position &pos, Tile *tile){
  //check to make sure this position is within bounds
  if(inBounds(pos)){
  //translate (x,y) coords into vector index
  int index = (pos->y * width) + pos->x;
  //replace old tile with new one and free memory
  Tile *old_tile = tiles[index];
  tiles[index] = tile;
  delete old_tile;
  }
  
}
const Tile * Maze::getTile(const Position &pos) const {
  //check to make sure this position is within bounds
  if(!inBounds(pos)){ return nullptr; }
  //translate (x,y) coord into vector index
  int index = (pos->y * width) + pos->x;
  //get tile at that index
  const Tile *t = tiles[index];
  return t;

}
static Maze * Maze::read(std::instream &in){
  //read in and store width and height of maze
  std::string w;
  std::string h;
  in >> w >> h;
  width = std::stoi(w);
  height = std::stoi(h);
  //read in and store maze contents
  int x = 0;
  std::string next_line;
  TileFactory *tfactory = TileFactory::getInstance();
  //iterate through each row and store its contents
  for(int y = 0; y < height; y++){
    x = 0;
    in >> next_line;
    std::stringstream ss(next_line);
    char cur;
    while(ss >> cur){
      Tile *new_tile = tfactory->createFromChar(cur);
      //if invalid tile is present, maze is invalid
      if(new_tile == nullptr){ return nullptr; }
      tiles.push_back(new_tile);
      x++;
    }
    //if the number of tiles in each row does not match the width, maze is invalid
    if(x != width - 1){ return nullptr; }
  }
  //return final maze
  return *this;
}
