// Lisa: unfinished
#include "basicgamerules.h"
#include <vector>

BasicGameRules::BasicGameRules() {}

BasicGameRules::~BasicGameRules() {}

bool BasicGameRules::allowMove(Game *g, Entity *e, const Position &from, const Position &to) const {
  // get the direction of getMoveDirection
  Direction d = (e->getController())->getMoveDirection(g, e);
  // get the current position to check
  Position curr = e->getPosition();
  // 1. check if the to position is floor
  // 2. check if it has already been occupied
  // by another entity of the same property
  MoveResult m = e->checkMoveOnto(e, from, to);
  if (m == MoveResults::ALLOW && !checkCollision(g, e, to)) { return 1; }
  // 3. check if the entity is moveable
  Entity *moveable = g->getEntityAt(to);
  Position new_to = to.displace(d);
  if (moveable->hasProperty('v') && moveable->checkMoveOnto(e, to, d) == MoveResults::ALLOW && !checkCollision(g, e, new_to)) { return 1; }
  return 0;
}

void enactMove(Game *g, Entity *e, const Position &to) const {
  // change the position
  e->m_pos = to;
}

// check the game result
GameResult checkGameResult(Game *g) const {
  // 1. if entity wth 'h' property reaches 
  // goal tile
  // TODO: EntityVec
  EntityVec heroes = g->getEntitiesWithProperty('h');
  EntityVec minotaurs = g->getEntitiesWithProperty('m');

  // check if the hero has been caught by the minotaur
  for (EntityVec::iterator it = heroes.begin();
		  it != heroes.end();
		  ++it) {
    for (EntityVec::iterator ptr = minotaurs.begin();
		    ptr != minotaurs.end();
		    ++it) {
      if ((*it)->getPosition() == (*ptr)->getPosition()) { return GameResult::HERO_LOSES; }
    }
  }

  // check if the goal has been reached by iterating
  // over the heroes
  for (EntityVec::iterator it = heroes.begin();
		  it != heroes.end();
		  ++it) {
    // if the position is the goal
    if (((*it)->getPosition()).isGoal()) { return GameResult::HERO_WINS; }
  }

  // game is unfinished
  return GameResult::UNKNOWN;
}

bool BasicGameRules::checkCollision(Game *g, Entity *e, const Position &to) {
  // TODO: is this legal? Can I use EntityVec here?
  const EntityVec& vec = g->getEntities();
  for (EntityVec::iterator it = vec->begin();
       it != vec->end();
       ++it) {
    // return 1 if there is collision, including 'v'
    if ((*it)->getPosition == to && e->getProperties() == (*it)->getProperties && (*it) != e) { return 1; }
  }
  // no collision
  return 0;
}

