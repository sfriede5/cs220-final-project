// Maddie Chabab & Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "ui.h"
#include "game.h"
#include "entity.h"
#include "uicontrol.h"

UIControl::UIControl() { }

UIControl::~UIControl() { }

Direction UIControl::getMoveDirection(Game *g, Entity *) {
  return (g->getUI())->getMoveDirection();
}

bool UIControl::isUser() const { return true; }

