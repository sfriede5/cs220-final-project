// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "tile.h"
#include "maze.h"
#include "entity.h"
#include "position.h"
#include "basicgamerules.h"
#include <vector>
#include <iterator>
#include <iostream>

using std::vector;
using std::iterator;
using std::cout;
using std::endl;

BasicGameRules::BasicGameRules() {}

BasicGameRules::~BasicGameRules() {}

bool BasicGameRules::allowMove(Game *g, Entity *e, const Position &from, const Position &to) const {
  // get the direction of getMoveDirection
  Direction d;
  if(from.displace(Direction::UP) == to){
      d = Direction::UP;
  } else if(from.displace(Direction::RIGHT) == to){
      d = Direction::RIGHT;
  } else if(from.displace(Direction::LEFT) == to){
      d = Direction::LEFT;
  } else {
      d = Direction::DOWN;
  }
  
  // get the current position to check
  //Position curr = e->getPosition();
  // 1. check if the to position is floor
  // 2. check if it has already been occupied
  // by another entity of the same property
  const Tile* dest_t = (g->getMaze())->getTile(to);
  if (!dest_t) { return 0; }
  MoveResult m = dest_t->checkMoveOnto(e, from, to);

  if (m == MoveResult::ALLOW && !checkCollision(g, e, to, d)) { return 1; }
  return 0;
}

void BasicGameRules::enactMove(Game *, Entity *e, const Position &to) const {
  // change the position
  e->setPosition(to);
}

// check the game result
GameResult BasicGameRules::checkGameResult(Game *g) const {

  // 1. if entity wth 'h' property reaches 
  // goal tile
  vector<Entity *> heroes = g->getEntitiesWithProperty('h');
  vector<Entity *> minotaurs = g->getEntitiesWithProperty('m');

  // check if the goal has been reached by iterating
  // over the heroes
  Maze *m = g->getMaze();
  for (vector<Entity *>::iterator it = heroes.begin();
		  it != heroes.end();
		  ++it) {
    // if the position is the goal
    if (m->getTile(((*it)->getPosition()))->isGoal()) { 
      return GameResult::HERO_WINS; 
    }
  }

  // check if the hero has been caught by the minotaur
  for (vector<Entity *>::iterator it = heroes.begin();
		  it != heroes.end();
		  ++it) {
    for (vector<Entity *>::iterator ptr = minotaurs.begin();
		    ptr != minotaurs.end();
		    ++ptr) {
      if ((*it)->getPosition() == (*ptr)->getPosition()) { return GameResult::HERO_LOSES; }
    }
  }

  // game is unfinished
  return GameResult::UNKNOWN;
}

bool BasicGameRules::checkCollision(Game *g, Entity *e, const Position &to, Direction d) const {
  // TODO: is this legal? Can I use EntityVec here?
  vector<Entity *> vec = g->getEntities();
  for (vector<Entity *>::iterator it = vec.begin();
       it != vec.end();
       ++it) {
    
    // if the positions coincide
    if ((*it)->getPosition() == to) {
      // check if the properties are the same
      if ((e->hasProperty('v') && (*it)->hasProperty('v')) ||
	  (e->hasProperty('h') && (*it)->hasProperty('h')) ||
	  (e->hasProperty('m') && (*it)->hasProperty('m'))) {
	// case 1: moveable hero pushing moveable
	// case 2: moveable minotaur pushing moveable
        if ((e->hasProperty('h') && (*it)->hasProperty('v')) ||
	    (e->hasProperty('m') && (*it)->hasProperty('v'))) {
	
          Position new_to = to.displace(d);
	  if (((g->getMaze())->getTile(new_to))->checkMoveOnto((*it), to, new_to) 
	  		  == MoveResult::ALLOW) {
	    return 0;
	  }
        }
	return 1; 
      }
      
      // case 1: moveable hero cannot pushing minotaur
      if ((e->hasProperty('h')) && (e->hasProperty('v')) && 
	  ((*it)->hasProperty('m'))) {
	return 1;
      }
      
      // case 2: moveable minotaur can devour hero
      if ((e->hasProperty('m')) && (e->hasProperty('v')) &&
	  ((*it)->hasProperty('h'))) {
        return 0;
      }

      // a moveable cannot push another entity
      if ((e->hasProperty('v') && (*it)->hasProperty('m')) ||
	  (e->hasProperty('v') && (*it)->hasProperty('h'))) {
        return 1;
      }

      // hero and minotaur pushing moveables
      if ((e->hasProperty('h') && (*it)->hasProperty('v')) ||
	  (e->hasProperty('m') && (*it)->hasProperty('v'))) {
	
        Position new_to = to.displace(d);
	if (((g->getMaze())->getTile(new_to))->checkMoveOnto((*it), to, new_to) 
			== MoveResult::ALLOW) {
	  return 0;
	}
	return 1;
      }
    }
  }
  // no collision
  return 0;
}

