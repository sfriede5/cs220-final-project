// Lisa Zhu
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include <vector>
#include <map>
#include "game.h"
#include "entity.h"
#include "astar.h"
#include "astarchasehero.h"

using std::vector;
using std::map;

AStarChaseHero::AStarChaseHero() {}

AStarChaseHero::~AStarChaseHero() {}

Direction AStarChaseHero::getMoveDirection(Game *g, Entity *e) {
  Position start = e->getPosition();
  EntityVec heroes = g->getEntitiesWithProperty('h');

  // use ordered map, K: the distance from e, V: the position
  // of hero
  
  map<int, Position> nearest_hero;

  for (EntityVec::iterator it = heroes.begin();
		  it != heroes.end();
		  ++it) {
    Position heroPos = (*it)->getPosition();
    int dist = heroPos.distanceFrom(start);
    nearest_hero[dist] = heroPos;
  }

  Position hero = nearest_hero.begin()->second;
  AStar *a = new AStar(g->getMaze(), start, hero);
  Direction dir = a->search(g, e);
  delete a;
  return dir;
}

bool AStarChaseHero::isUser() const { return 0; }
