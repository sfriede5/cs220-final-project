// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "cursesui.h"
#include "position.h"
#include "game.h"
#include "maze.h"
#include "entity.h"
#include "tile.h"
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#define FLOOR 1
#define WALL 2
#define GOAL 3
#define M 4
#define H 5
#define I 6
#define LOSE 7
#define WIN 8
#define ILLEGAL 9

using std::vector;
using std::string;
using std::stringstream;
using std::cout;
using std::cin;
using std::endl;

typedef vector<Entity *> EntityVec;

CursesUI::CursesUI() {}

CursesUI::~CursesUI() {}

Direction CursesUI::getMoveDirection() {
  char c = 0;
  Direction d;

  switch (c = getch()) {
    case 'u':
      d = Direction::UP;
      break;
    case 'd':
      d = Direction::DOWN;
      break;
    case 'l':
      d = Direction::LEFT;
      break;
    case 'r':
      d = Direction::RIGHT;
      break;
    default:
      return getMoveDirection();
  }
  return d;
}

void CursesUI::displayMessage(const string &msg, bool b) {
  this->m_msg = msg;
  this->m_endgame = b;
}

void CursesUI::render(Game *game) {
  erase();
  vector<char> glyphs;
  Maze *maze = game->getMaze();
  int width = maze->getWidth();
  int height = maze->getHeight();
  EntityVec ent = game->getEntities();

  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      Position p(x, y);
      const Tile *tile = maze->getTile(p);
      string glyph = tile->getGlyph();
      if (glyph == ".") {
        attron(COLOR_PAIR(FLOOR));
        mvaddch(y, x, '.');
        attroff(COLOR_PAIR(FLOOR));
      } else if (glyph == "#") {
	attron(COLOR_PAIR(WALL));
	mvaddch(y, x, '#');
	attroff(COLOR_PAIR(WALL));
      } else if (glyph == "<") {
	attron(COLOR_PAIR(GOAL));
	mvaddch(y, x, '<');
	attroff(COLOR_PAIR(GOAL));
      }
    }
  }

  for (EntityVec::const_iterator it = ent.cbegin();
		  it != ent.cend();
		  it++) {
    Position p = (*it)->getPosition();
    char glyph = ((*it)->getGlyph()).at(0);
    int y = p.getY();
    int x = p.getX();
    if ((*it)->hasProperty('h')) {
      attron(COLOR_PAIR(H));
      mvaddch(y, x, glyph);
      attroff(COLOR_PAIR(H));
    } else if ((*it)->hasProperty('m')) {
      attron(COLOR_PAIR(M));
      mvaddch(y, x, glyph);
      attroff(COLOR_PAIR(M));
    } else if ((*it)->hasProperty('v')) {
      attron(COLOR_PAIR(I));
      mvaddch(y, x, glyph);
      attroff(COLOR_PAIR(I));
    }
  }

  string new_space = "";
  mvprintw(height, 0, new_space.c_str());
  if (!(this->m_msg.empty()) || !(this->m_msg == "")) {
    string new_msg = ": " + this->m_msg + "\n";
    if (this->m_msg == "Hero wins") {
      attron(COLOR_PAIR(WIN));
      attron(A_BOLD);
      mvprintw(height + 1, 0, "%s", new_msg.c_str());
      attroff(A_BOLD);
      attroff(COLOR_PAIR(WIN));
    } else if (this->m_msg == "Hero loses") {
      attron(COLOR_PAIR(LOSE));
      attron(A_BOLD);
      mvprintw(height + 1, 0, "%s", new_msg.c_str());
      attroff(A_BOLD);
      attroff(COLOR_PAIR(LOSE));  
    } else if (this->m_msg == "Illegal move") {
      attron(COLOR_PAIR(ILLEGAL));
      attron(A_BLINK);
      mvprintw(height + 1, 0, "%s", new_msg.c_str());
      attroff(A_BLINK);
      attroff(COLOR_PAIR(ILLEGAL));
    } else {
      mvprintw(height + 1, 0, "%s", new_msg.c_str());
    }

    (this->m_msg).clear();
  }
}

