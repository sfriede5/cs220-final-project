// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#ifndef CURSESUI_H
#define CURSESUI_H

#include "ui.h"
#include <ncurses.h>
#include <string>

class CursesUI : public UI {
  private:
    // fields
    std::string m_msg;
    bool m_endgame;
    // copy constructor and assignment operator disallowed
    CursesUI(const CursesUI &);
    CursesUI &operator=(const CursesUI &);

  public:
    CursesUI();
    virtual ~CursesUI();
    virtual Direction getMoveDirection();
    virtual void displayMessage(const std::string &msg, bool endgame);
    virtual void render(Game *game);
};

#endif // CURSESUI_H
