// Sydney Friedel
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "maze.h"
#include "ui.h"
#include "tile.h"
#include "position.h"
#include "entity.h"
#include "gamerules.h"
#include "entitycontroller.h"
#include "ecfactory.h"
#include "game.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <iostream>

using std::cout; using std::endl;

typedef std::vector<Entity *> EntityVec;
Game::Game(){
}

Game::~Game(){
  delete this->m_maze;
  delete this->m_ui;
  delete this->m_gameRules;
  for(EntityVec::iterator it = this->m_entities.begin();
      it != this->m_entities.end(); it++){
    delete *it;
  }
}

void Game::setMaze(Maze *maze){
  this->m_maze = maze;
}

void Game::setUI(UI *ui){
  this->m_ui = ui;
}

void Game::setGameRules(GameRules *gameRules){
  this->m_gameRules = gameRules;
}

void Game::addEntity(Entity *entity){
  this->m_entities.push_back(entity);
}

Entity * Game::getEntityAt(const Position &pos){
  
  //iterate through each of the entities and compare their pos
  //to the specified one
  for(EntityVec::iterator it = this->m_entities.begin();
      it != this->m_entities.end(); it++){
    
    Position cur_pos = (*it)->getPosition();
   
    //if this entity's current position equals the specified entity
    //return this entity
    if(cur_pos == pos){
     
      return *it; }
  }
  //if we got this far, none of the entities are at that position
 
  return nullptr;
}

const EntityVec & Game::getEntities() const{
  return this->m_entities;
}

EntityVec Game::getEntitiesWithProperty(char prop) const{
  //EntityVec to hold entities with property
  EntityVec vec;
  //iterator through this game's entities and find the ones with property
  for(EntityVec::const_iterator it = this->m_entities.cbegin();
      it != this->m_entities.cend(); it++){
    //if it has that property, add it to the new EntityVec
    if((*it)->hasProperty(prop)){ vec.push_back(*it); }
  }
  return vec;
}

Maze * Game::getMaze(){ return this->m_maze; }

UI * Game::getUI(){ return this->m_ui; }

GameRules * Game::getGameRules(){ return this->m_gameRules; }

void Game::gameLoop(){
  
  //create iterator to be updated in round robin fashion
  EntityVec::iterator it = this->m_entities.begin();
  //get initial game result (should be UNKNOWN)
  GameResult game_result = this->m_gameRules->checkGameResult(this);
  //continue taking turns until game result changes from UNKNOWN
  while(game_result == GameResult::UNKNOWN){
    
    //if it's the hero's turn, display game state
    if((*it)->getController()->isUser()){
      this->m_ui->render(this);
    }
    //allow the current entity to take a turn
    this->takeTurn(*it);
    //update gameResult
    game_result = this->m_gameRules->checkGameResult(this);
    //figure out which entity goes next
    if(it + 1 == m_entities.end()){
      it = m_entities.begin();
    } else {
      it++;
    }
  }
  //display final game state
  if(game_result == GameResult::HERO_WINS){
    std::string msg = "Hero wins";
    this->m_ui->displayMessage(msg, true);
    this->m_ui->render(this);
  } else {
    std::string msg = "Hero loses";
    this->m_ui->displayMessage(msg, true);
    this->m_ui->render(this);
  }
}

void Game::takeTurn(Entity *actor){  
  //get proposed move from entity controller
  Direction dir = actor->getController()->getMoveDirection(this, actor);
  //See if gameRules allows move
  Position from = actor->getPosition();
  Position dest = from.displace(dir);
  
  bool allowed = this->m_gameRules->allowMove(this, actor, from, dest);
  //if move is allowed, enact the move
  if(allowed){
    Entity *new_e = this->getEntityAt(dest);
    Position new_dest = dest.displace(dir);
    if (new_e && new_e->hasProperty('v')) {
      bool new_allowed = this->m_gameRules->allowMove(this, new_e, dest, new_dest);
      if (new_allowed) { 
	this->m_gameRules->enactMove(this, new_e, new_dest); 
        this->m_gameRules->enactMove(this, actor, dest);
      }
    } else {
      this->m_gameRules->enactMove(this, actor, dest);
    }
    // then check the moveable
  } else if(actor->getController()->isUser()){
    std::string msg = "Illegal move";
    this->m_ui->displayMessage(msg, false);   
  }
}

Game * Game::loadGame(std::istream &in){
  //read in and store maze contents
  Maze *m = Maze::read(in);  
  if(m == nullptr){
    // delete m;
    return nullptr;
  }
  Game *game = new Game();
  game->setMaze(m);
  //read in and store entity contents
  EntityControllerFactory *ecfactory = EntityControllerFactory::getInstance();
  std::string next_code;
  int x = 0;
  int y = 0;
 
  while (in) {
    if (in >> next_code >> x >> y) {
      Position pos(x, y);
      //check to make sure game data is valid; update: not necessary according to project guidelines
      if(!game->m_maze->inBounds(pos)){
        //tring to get rid of memory leaks and conditional jump errors
        game->setUI(nullptr);
        game->setGameRules(nullptr);
        delete game;
        return nullptr;
      }
    
      //create new entity and add it to m_entities
      Entity *new_ent = game->decodeEntity(next_code, x, y, ecfactory);
      game->addEntity(new_ent);
    }
  }
  return game;
}

Entity * Game::decodeEntity(std::string code, int x, int y,
			    EntityControllerFactory *ecfactory){
  
  Entity *ent = new Entity;
  char gly = code.at(0);
  std::string glyph(1, gly);
  ent->setGlyph(glyph);
  char ec = code.at(1);
  EntityController *controller = ecfactory->createFromChar(ec);
  ent->setController(controller);
  if(code.length() >= 3){
    std::string props = code.substr(2);
    ent->setProperties(props);
  } else {
    ent->setProperties("");
  }
  Position pos(x, y);
  ent->setPosition(pos);
  return ent;
  

}
