// Sydney Friedel
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "tile.h"
#include "entity.h"
#include "position.h"
#include "tilefactory.h"
#include "maze.h"
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>


using std::cout; using std::endl;

Maze::Maze(int w, int h){
  this->width = w;
  this->height = h;
  for(int i = 0; i < (width*height); i++){
    this->tiles.push_back(NULL);
  }
}

Maze::~Maze(){
  for(std::vector<Tile*>::iterator it = tiles.begin();
      it != tiles.end(); it++){
    if (*it != NULL){
      delete *it;
    }
  }
  
}

int Maze::getWidth() const {return this->width; }
int Maze::getHeight() const { return this->height;} 

bool Maze::inBounds(const Position &pos) const{
  return ((pos.getX() >= 0) && (pos.getX() < width)
	  && (pos.getY() >= 0) && (pos.getY() < height));				       
}

void Maze::setTile(const Position &pos, Tile *tile){
  //check to make sure this position is within bounds
  if(this->inBounds(pos)){
  //translate (x,y) coords into vector index
    int index = (pos.getY() * width) + pos.getX();
  //replace old tile with new one and free memory
    if(this->tiles[index] != NULL){
      Tile *old_tile = tiles[index];
      delete old_tile;
    }
    tiles[index] = tile;
  }
}
const Tile * Maze::getTile(const Position &pos) const {
  //check to make sure this position is within bounds
  if(!this->inBounds(pos)){return nullptr; }
  //translate (x,y) coord into vector index
  int index = (pos.getY() * width) + pos.getX();
  //get tile at that index
  const Tile *t = this->tiles[index];
  return t;

}
Maze * Maze::read(std::istream &in){
  //read in and store width and height of maze
  int width = 0;
  int height = 0;
  in >> width >> height;
  if(width == 0 || height == 0){ return nullptr; }
  Maze *maze = new Maze(width, height);
  //read in and store maze contents
  int x = 0;
  std::string next_line;
  TileFactory *tfactory = TileFactory::getInstance();
  //iterate through each row and store its contents
  for(int y = 0; y < height; y++){
    x = 0;
    in >> next_line;
    std::stringstream ss(next_line);
    char cur;
    while(ss >> cur){
      Tile *new_tile = tfactory->createFromChar(cur);
      if(x == width){
	delete new_tile;
	delete maze;
	return nullptr;
      }
      //if invalid tile is present, maze is invalid
      if(new_tile == nullptr){
	delete new_tile;
	delete maze;
	return nullptr;
      }
      maze->setTile(Position(x,y),new_tile);
      x++;
    }
    //if the number of tiles in each row does not match the width, maze is invalid
    if(x != width){
      delete maze;
      return nullptr; }
  }
  
  //return final maze
  return maze;
}

