// Maddie Chabab
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "position.h"
#include "game.h"
#include "maze.h"
#include "entity.h"
#include "tile.h"
#include "textui.h"
#include <iterator>
#include <iostream>

using std::cout; using std::cin; using std::endl;

typedef std::vector<Entity *> EntityVec;

TextUI::TextUI () { }

TextUI::~TextUI() { }

Direction TextUI::getMoveDirection() {
  // updating here to be consistent with ncurses
  //char userMove;
  std::string user;
  cout << "Your move (u/d/l/r): ";
  cin >> user;
  char userMove = user.at(0);
  if (userMove == 'u') {
    Direction d = Direction::UP;
    return d; 
  }
  else if (userMove == 'd') {
    Direction d = Direction::DOWN;
    return d;
  }
  else if (userMove == 'l') {
    Direction d = Direction::LEFT;
    return d;
  }
  else if (userMove == 'r') {
    Direction d = Direction::RIGHT;
    return d;
  }
  else {
    cout << "Unknown direction";
    cout << endl;
    return(getMoveDirection());
  }
}

void TextUI::displayMessage(const std::string &msg, bool endgame) {
  (void)endgame;
  this->m_msg = msg; //message is not displayed until render is called
  //bool endgame unused in regular version of game
}

void TextUI::render(Game *game) {
  //create vector of chars to hold glyphs for maze tiles and entities
  std::vector<char> glyphs;
  //fill vector of chars with maze tiles
  Maze *maze = game->getMaze();
  int width = maze->getWidth();
  int height = maze->getHeight();
  for(int y = 0; y < height; y++){
    for(int x = 0; x < width; x++){
      Position p(x, y);
      const Tile *tile = maze->getTile(p);
      std::string cur_glyph = tile->getGlyph();
      char cur_gly = cur_glyph.at(0);
      glyphs.push_back(cur_gly);
    }
  }
  //overwrite tile glyphs with whatever entities are on those spots
  for (EntityVec::const_iterator it = game->getEntities().cbegin();
       it != game->getEntities().cend(); it++) {
    Position p = (*it)->getPosition();
    int index = (p.getY() * width) + p.getX();
    std::string cur_glyph = (*it)->getGlyph();
    char cur_gly = cur_glyph.at(0);
    glyphs[index] = cur_gly;
  }
  //print current game state
  int count = 0;
  for(std::vector<char>::iterator it = glyphs.begin(); it != glyphs.end(); it++){
    cout << *it;
    count++;
    if(count % width == 0){ cout << endl; }
  }
  //display any waiting message
  if ((!this->m_msg.empty()) || !(this->m_msg == "")) {
    cout << ": " << this->m_msg;
    cout << endl;
    (this->m_msg).clear();
  } 
}
 
