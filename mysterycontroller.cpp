// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "mysterycontroller.h"

MysteryController::MysteryController() {
}

MysteryController::~MysteryController() {
}

Direction MysteryController::getMoveDirection(Game *, Entity *) {
  return Direction::NONE;
}

bool MysteryController::isUser() const {
  return false;
}
