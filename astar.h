// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#ifndef ASTAR_H
#define ASTAR_H

#include <vector>
// forward declaration
class Maze;
class Game;
class Entity;
class Position;
class GameRules;

// Bundle struct groups together
// a Position's previous position that
// it came from, and the f, g, and h values
// associated with the Position
struct Bundle {
  Position prev_pos;
  int f, g, h;
};

// Pair stores a Position and its f-value
typedef std::pair<int, Position> Pair;

class AStar {
  private:
    // private fields
    Maze *m_maze;
    Position m_start;
    Position m_goal; // closest hero

    // copy constructor and assignment operator disallowed
    AStar(const AStar &);
    AStar &operator=(const AStar &);
    
    // manhattan distance: abs values of differences in the
    // goal's x and y coordinates and the current cell's x
    // and y coordinates
    // Position: int distanceFrom(const Position &other) const {}
    int h(Position &curr, Position &goal) const;
    int posToIndex(Position &p);
    Direction trace(std::vector<Bundle> b);
  public:
    AStar(Maze *maze, Position &m_start, Position &m_goal); // constructor
    ~AStar(); // destructor
    Direction search(Game *game, Entity *entity); // path-search
    bool allowMove(Game *game, Entity *entity, Position &from, Position &to); 
    // checks whether move is allowed
};

#endif // ASTAR_H
