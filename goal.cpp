// Sydney Friedel
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "goal.h"
#include "entity.h"
#include "position.h"
#include "tile.h"
#include <string>

Goal::Goal() { }

Goal::~Goal() { }

MoveResult Goal::checkMoveOnto(Entity *, const Position &fromPos, const Position &tilePos) const {

  if(fromPos.distanceFrom(tilePos) == 1){ return MoveResult::ALLOW; }
  return MoveResult::BLOCK;
}

bool Goal::isGoal() const { return true; }

std::string Goal::getGlyph() const {
  std::string str = "<";
  return str;
}
