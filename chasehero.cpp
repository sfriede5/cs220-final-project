// Maddie Chabab
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "game.h"
#include "entity.h"
#include "gamerules.h"
#include "chasehero.h"
#include <vector>
#include <math.h>
#include <iterator>
#include <iostream>

using std::cout; using std::endl;

typedef std::vector<Entity *> EntityVec;

ChaseHero::ChaseHero() { }

ChaseHero::~ChaseHero() { }

Direction ChaseHero::getMoveDirection(Game *game, Entity *entity) {
  EntityVec heros = game->getEntitiesWithProperty('h');
  int count = 0;
  int prevDist, currDist;   
  int currX, currY, prevX, prevY;
  int heroX, heroY;
  
  //Minotaur position
  Position mPos = entity->getPosition();
  const Position& minotaurPos = mPos;
  
  Entity *first_ent = heros[0];
  prevDist = (first_ent->getPosition()).distanceFrom(mPos);
  prevX = (first_ent->getPosition()).getX();
  prevY = (first_ent->getPosition()).getY();
  heroX = prevX;
  heroY = prevY;
  
  //Determine closest hero and its x,y coordinates  
  for (EntityVec::iterator it = heros.begin();
       it != heros.end(); ++it) {
    //assuming "shortest" as determined by distanceFrom of position class
    currDist = ((*it)->getPosition()).distanceFrom(mPos);
    currX = ((*it)->getPosition()).getX();
    currY = ((*it)->getPosition()).getY();
    if (count != 0) {
      if (currDist < prevDist) {
	heroX  = currX;
	heroY  = currY; 
      }
      else {
	heroX  = prevX;
	heroY  = prevY;
      }
      //find x and y of closest hero
    }
    prevDist = currDist;
    prevX = currX;
    prevY = currY;
    count++;
  }
  
  //Determine recommended move for Minotaur
  int minotaurX = (mPos).getX();
  int minotaurY = (mPos).getY();
  Direction minoMove = Direction::NONE;
  Position u = mPos.displace(Direction::UP);
  const Position& destU = u;
  Position d = mPos.displace(Direction::DOWN);
  const Position& destD = d; 
  Position l = mPos.displace(Direction::LEFT);
  const Position& destL = l;
  Position r = mPos.displace(Direction::RIGHT);
  const Position& destR = r;
  
  if ( abs(minotaurY - heroY) > abs(minotaurX - heroX) ) { 
    //Hero is farther vertically (larger difference in Y coordinates)  
    if (heroY <  minotaurY) {
      minoMove = Direction::UP;
      //if minotaur cannot move up 
      if (!(this->canMove(game, entity, minotaurPos, destU))) {
	if (heroX < minotaurX) {
	  minoMove = Direction::LEFT;
	  if (!(this->canMove(game, entity, minotaurPos, destL))) {
	    minoMove = Direction::NONE;
	  }
	}
	else if (heroX > minotaurX) {
	  minoMove = Direction::RIGHT;
	  if (!(this->canMove(game, entity, minotaurPos, destR))) {
	    minoMove = Direction::NONE;
	  }
	}
      }
      return minoMove;
    }
    else if (heroY > minotaurY) {
      minoMove = Direction::DOWN;
      //if minotaur cannot move down
      if (!(this->canMove(game, entity, minotaurPos, destD))) {
        if (heroX < minotaurX) {
          minoMove = Direction::LEFT;
          if (!(this->canMove(game, entity, minotaurPos, destL))) {
            minoMove = Direction::NONE;
          }	  
        }
        else if (heroX > minotaurX) {
          minoMove = Direction::RIGHT;
          if (!(this->canMove(game, entity, minotaurPos, destR))) {
            minoMove = Direction::NONE;
          }
        }
      }
      return minoMove;
    }
  }
  else if ( abs(minotaurX - heroX) > abs(minotaurY - heroY) ||
	    abs(minotaurX - heroX) == abs(minotaurY - heroY) ) { 
    //Hero is either farther horizontally or equally far horizontal/vertical
    if (heroX < minotaurX) {
      minoMove = Direction::LEFT;
      //if minotaur cannot move left 
      if (!(this->canMove(game, entity, minotaurPos, destL))) {
        if (heroY < minotaurY) {
          minoMove = Direction::UP;
          if (!(this->canMove(game, entity, minotaurPos, destU))) {
            minoMove = Direction::NONE;
          }
        }
        else if (heroY > minotaurY) {
          minoMove = Direction::DOWN;
          if (!(this->canMove(game, entity, minotaurPos, destD))) {
            minoMove = Direction::NONE;
          }
        }
      }
      return minoMove;
    }
    else if (heroX > minotaurX) {
      minoMove = Direction::RIGHT;
      //if minotaur cannot move right
      if (!(this->canMove(game, entity, minotaurPos, destR))) {
        if (heroY < minotaurY) {
          minoMove = Direction::UP;
          if (!(this->canMove(game, entity, minotaurPos, destU))) {
            minoMove = Direction::NONE;
          }	  
        }      
        else if (heroY > minotaurY) {
          minoMove = Direction::DOWN;
          if (!(this->canMove(game, entity, minotaurPos, destD))) {
            minoMove = Direction::NONE;
          }
        }
        return minoMove;
      }
      return minoMove;
    }
  }
  return Direction::NONE;
}


bool ChaseHero::isUser() const { return false; }

bool ChaseHero::canMove(Game *game, Entity *minotaur, const Position &minotaurPos, const Position &dest) const { 
  GameRules *theseRules = game->getGameRules();
  bool allowed = theseRules->allowMove(game, minotaur, minotaurPos, dest);
  return allowed; 
}
