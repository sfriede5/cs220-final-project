// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include <iostream>
#include <fstream>
#include "game.h"
#include "cursesui.h"
#include "maze.h"
#include "basicgamerules.h"

#define FLOOR 1
#define WALL 2
#define GOAL 3
#define MINOTAUR 4
#define HERO 5
#define INANIMATE 6
#define LOSE 7
#define WIN 8
#define ILLEGAL 9

int main(int argc, char *argv[]){
  const char *filename;
  if(argc < 2){
    std::cerr << "Error: Need filename" << std::endl;
    return 1;
  }
  filename = argv[1];

  std::ifstream in(filename);
  if(!in.is_open()){
    std::cerr << "Error: could not open maze file" << std::endl;
    return 1;
  }
  Game *game = Game::loadGame(in);
  if(!game){
    std::cerr << "Error: Invalid game file" << std::endl;
    return 1;
  }
  game->setUI(new CursesUI());
  game->setGameRules(new BasicGameRules());
  if ((game->getEntities()).empty()) { 
    std::cerr << "Error: Invalid game file" << std::endl;
    delete game;
    return 1; 
  }

  // ---------- start ncurses ---------- //
  initscr();
  start_color();
  
  init_pair(FLOOR, COLOR_YELLOW, COLOR_YELLOW); // floor
  init_pair(WALL, COLOR_BLUE, COLOR_BLUE); // wall
  init_pair(GOAL, COLOR_WHITE, COLOR_BLACK); // goal
  init_pair(MINOTAUR, COLOR_RED, COLOR_YELLOW); // minotaur
  init_pair(HERO, COLOR_BLACK, COLOR_YELLOW); // hero
  init_pair(INANIMATE, COLOR_MAGENTA, COLOR_MAGENTA); // moveable
  init_pair(LOSE, COLOR_RED, COLOR_BLACK); // hero loses message
  init_pair(WIN, COLOR_GREEN, COLOR_BLACK); // hero wins message
  init_pair(ILLEGAL, COLOR_YELLOW, COLOR_BLACK); // illegal move message

  cbreak();
  noecho();
  clear();
  

  game->gameLoop();

  int rows = (game->getMaze())->getHeight() + 3;
  WINDOW *new_w = newwin(3, 37, rows, 0);
  refresh();

  wborder(new_w, '*', '*', '*', '*', '*', '*', '*', '*');
  touchwin(new_w);
  wrefresh(new_w);
  //attron(A_STANDOUT);
  //mvaddstr(rows, 0, "GAME OVER, PRESS ANY KEY TO QUIT");
  mvwprintw(new_w, 1, 2, "GAME OVER, PRESS ANY KEY TO QUIT:");
  //attroff(A_STANDOUT);


  // clean up ncurses
  refresh();
  wrefresh(new_w);
  getch();
  delwin(new_w);
  endwin();
  // ---------- end ncurses ---------- //
  
  delete game;
  return 0;
}
