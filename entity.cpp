// Lisa
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include <string>
#include "entitycontroller.h"
#include "entity.h"
using std::string;
using std::size_t;

#include <iostream>
using std::cout; using std::endl;

Entity::Entity() : m_pos() {
  m_controller = nullptr;
  m_glyph = "";
  m_props = "";
}

Entity::~Entity() { delete m_controller; }

void Entity::setGlyph(const string &glyph) {
  this->m_glyph = glyph;
}

void Entity::setProperties(const string &props) {
  this->m_props = props;
}

string Entity::getGlyph() const {
  return this->m_glyph;
}

string Entity::getProperties() const {
  return this->m_props;
}

bool Entity::hasProperty(char prop) const {
  size_t found = m_props.find(prop);
  return found != string::npos;
}

void Entity::setController(EntityController *controller) {
  this->m_controller = controller;
}

EntityController* Entity::getController() {
  return this->m_controller;
}

void Entity::setPosition(const Position &pos) {
  this->m_pos = pos;
}


Position Entity::getPosition() const {
 
  return this->m_pos;
}
