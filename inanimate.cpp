// Sydney Friedel
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "inanimate.h"

Inanimate::Inanimate() {}

Inanimate::~Inanimate() {}

Direction Inanimate::getMoveDirection(Game *, Entity *){
  return Direction::NONE;
}

bool Inanimate::isUser() const{ return 0; }
