// Sydney Friedel
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "floor.h"
#include "position.h"
#include "entity.h"
#include "tile.h"
#include <string>

Floor::Floor() { }
Floor::~Floor() { }
MoveResult Floor::checkMoveOnto(Entity *, const Position &fromPos
					,const Position &tilePos) const {
  // check how much the entity is moving, cannot be 
  // greater than 1
  if (fromPos.distanceFrom(tilePos) == 1) {
    return MoveResult::ALLOW;
  }
  return MoveResult::BLOCK;
}

bool Floor::isGoal() const { return false; }

std::string Floor::getGlyph() const {
  std::string str = ".";
  return str;
}
