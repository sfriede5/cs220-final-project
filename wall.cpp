// Sydney Friedel
// Project Team:
//   Maddie Chabab; mchabab1
//   Sydney Friedel; sfriede5
//   Lisa Zhu; czhu26

#include "wall.h"
#include "position.h"
#include "entity.h"
#include "tile.h"
#include <string>

Wall::Wall(){ }

Wall::~Wall(){ }

MoveResult Wall::checkMoveOnto(Entity *, const Position &,
				       const Position &) const {
  return MoveResult::BLOCK;
  
}

bool Wall::isGoal() const { return false; }

std::string Wall::getGlyph() const {
  std::string str = "#";
  return str;
}

